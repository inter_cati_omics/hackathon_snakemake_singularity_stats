# Snakemake stats singularity image  
  
This receipe is for creating a single image containing matplotlib=3.1, pysam=0.15 along with python=3.8 for the snakemake short tutorial.  
  
This singularity image is automatically built (gitlab-ci) and pushed on the registry using .gitlab-ci.yml file.  
  
This image can be pulled (using singularity >= 3.3) with:  
  
```bash
SINGULARITY_DISABLE_CACHE=0 singularity --debug pull snakemake_mapping.sif oras://registry.forgemia.inra.fr/inter_cati_omics/hackathon_snakemake_singularity_stats/hackathon_snakemake_singularity_stats:latest

``` 

Or you can download directly the singularity-image: artefacts.zip

Or you can use singularity directive in Snakefile (in global or rule scope) to point to oras url.  
This way, if you run snakemake using `--use-singularity` option, it will snakemake to deploy singularity image at this url to run the workflow.  
  
```python
# Snakefile
singularity: "oras://registry.forgemia.inra.fr/inter_cati_omics/hackathon_snakemake_singularity_stats/hackathon_snakemake_singularity_stats:latest"

``` 

then, run snakemke with `--use-singularity` option.  
  
